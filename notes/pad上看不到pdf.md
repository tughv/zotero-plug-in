如果你在pad上安装了zotero，也用了webdev和官方同步的解决方案，但是在平板上只能看到条目信息却打不开pdf。

检查一下自己是不是用了zotfile并做了如下配置，在`工具-zotfile preference`里面看。

![image-20220415191718049](pad上看不到pdf.assets/image-20220415191718049.png)

如果你选了`Custom Location`，那么pdf就会被移动到你选择的那个文件夹，而不是`zotero`保存Pdf的文件夹，看起来的pdf就是下面第二种这种链接形式，而不是第一种PDF形式。

![image-20220415192437950](pad上看不到pdf.assets/image-20220415192437950.png)

所以需要把zotfile按照上面那种勾选`Attach stored copy of file(s)`，然后再把移动过的pdf搬回去。

单选或者全选你的条目，鼠标右键，如下。

![image-20220415192632188](pad上看不到pdf.assets/image-20220415192632188.png)

所以如果不必要修改pdf的名字可以将zotfile插件卸载了。