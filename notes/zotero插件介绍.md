所有的插件都在`zotero插件/适配Zotero 6.0 插件`的文件夹里面

## zotfile插件

[zotfile插件说明](zotfile插件说明.md)

## 茉莉花插件jasminum

[中文pdf自动识别归类-茉莉花插件](茉莉花插件.md)

## pdf翻译插件zotero-pdf-translate

安装该插件可以实现在zotero里面翻译pdf

在zotero `编辑-首选项`里面的插件页面进行参数配置

![image-20220623173519843](zotero插件介绍.assets/image-20220623173519843.png)

即可以在pdf页面进行翻译了

## [自动创建笔记模板zotero-mdnotes](Zotero-Mdnotes插件模板教程.html)

网址：https://zhuanlan.zhihu.com/p/394682612

